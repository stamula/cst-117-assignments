using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryApp
{
    public partial class Form1 : Form
    {

        ItemList allitems = new ItemList();

        public Form1()
        {
            InitializeComponent();
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2(allitems.Items);

            this.Hide();
            form2.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Item item1 = new Item(1, 10, "Tumbled Rose Quartz Crystals");

            allitems.addItem(item1);

            foreach (Item item in allitems.Items)
            {
                lbInventory.Items.Add(item.ToString());
            }
        }
    }
}
