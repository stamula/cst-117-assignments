using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp
{
    public class ItemList
    {
        private List<Item> items;

        internal List<Item> Items { get => items; set => items = value; }

        public ItemList()
        {
            items = new List<Item>();
        }

        public void addItem(Item i)
        {
            Items.Add(i);
        }

        public void deleteItem(Item i)
        {
            Items.Remove(i);
        }

    }
}
