using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InventoryApp
{
    public partial class Form2 : Form
    {
        public List<Item> allitems;

        public Form2(List<Item> listofitems)
        {
            allitems = listofitems;
            InitializeComponent();
        }

        private void BtnAddItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach(Item item in allitems)
                {
                    if (item.ID == nudId.Value)
                    {
                        MessageBox.Show("ID already exists!");
                    }
                    else if (item.Name == tbName.Text)
                    {
                        MessageBox.Show("Item name already exists!");
                    }
                }

                Convert.ToInt32(nudId.Value);
                Convert.ToDouble(nudPrice.Value);
                Convert.ToInt32(nudQuantity.Value);
            }
            catch
            {

            }
        }
    }
}
