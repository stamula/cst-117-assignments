using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp
{
    public class Item
    {
        private int id, quantity;
        private string name;

        public int ID { get { return id; } set { id = value; } }
        public int Quantity {  get { return quantity; } set { quantity = value; } }
        public string Name { get { return name;  } set { name = value; } }

        public Item()
        {
            id = -1;
            quantity = -1;
            name = "undefined";
        }

        public Item(int input_id, int input_quantity, string input_name)
        {
            ID = input_id;
            Quantity = input_quantity;
            Name = input_name;
        }

        public override string ToString()
        {
            return "ID: " + ID + "; Quantity: " + Quantity + "; Name: " + Name;
        }
    }
}
